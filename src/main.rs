use fltk::{
	app, enums::*, prelude::*,
	window::Window, input::Input,
	text::{TextDisplay, TextBuffer, WrapMode}
};
use fltk_theme::{ColorTheme, color_themes, WidgetTheme, ThemeType};
use std::{
	time::Duration, thread, env,
	io::{Write, Read},
	net::{TcpStream, ToSocketAddrs, SocketAddr}
};

static mut ID: usize = 0;

fn timeout() {
	app::repeat_timeout2(1.0, timeout);
}

fn spawn_read_updates(addr: SocketAddr, room: String, mut output: TextDisplay) {
	let key = ["update", "\n", unsafe { &ID.to_string() }, "\n", &room].join("");
	
	let mut stream = TcpStream::connect(addr).unwrap();
	stream.write(&key.as_bytes()).unwrap();
	let string = read_reply(&mut stream);
	
	output.buffer().unwrap().append(&string);
	output.scroll(
		output.count_lines(
			0,
			output.buffer().unwrap().length(), true
		),
		0,
	);
	
	thread::spawn(
		move || loop {
			let key = ["update", "\n", unsafe { &ID.to_string() }, "\n", &room].join("");
			let mut stream = TcpStream::connect(addr).unwrap();
			stream.write(&key.as_bytes()).unwrap();
			let string = read_reply(&mut stream);
			output.buffer().unwrap().append(&string);
			output.scroll(
				output.count_lines(
					0,
					output.buffer().unwrap().length(), true
				),
				0,
			);
			thread::sleep(Duration::from_millis(1000));
		}
	);
}

fn read_reply(mut stream: &TcpStream) -> String {
	let mut string = String::new();
	let mut buffer = [0; 64000];
	let data = stream.read(&mut buffer[0..64000]);
	match data {
		Ok(data) => {
			let input = String::from_utf8_lossy(&buffer[0..data]).to_string();
			if input.lines().count() > 1 {
				let mut lines = input.lines();
				match lines.next().unwrap().parse::<usize>() {
					Ok(id) => {
						unsafe { ID = id };
						string = lines.collect::<Vec<&str>>().join("\n") + &'\n'.to_string();
					}
					Err(_) => (),
				}
			}
		}
		Err(_) => (),
	}
	string
}

fn main() {
	let domain = "archneek.zapto.org:14114";
	let addr = domain.to_socket_addrs().unwrap().next().unwrap();
	let room = {
		match env::args().nth(1) {
			Some(room) => room.to_string(),
			None => "neek".to_string(),
		}
	};
	
	let app = app::App::default().with_scheme(app::Scheme::Gtk);
	app::add_timeout2(1.0, timeout);
	
	let theme = ColorTheme::new(color_themes::BLACK_THEME);
	theme.apply();
	
	let widget_theme = WidgetTheme::new(ThemeType::Dark);
	widget_theme.apply();
	
	let mut wind = Window::new(100, 100, 400, 300, "Hello from rust");

	let mut output = TextDisplay::default().with_size(400, 250);
	let buf = TextBuffer::default();
	output.set_buffer(buf);
	output.wrap_mode(WrapMode::AtBounds, 0);
	output.visible_focus(false);
	spawn_read_updates(addr, room.clone(), output.clone());
	
	let mut input = Input::default();
	input.set_size(400, 30);
	input.set_pos(0, 270);
	
	input.handle(move |i, ev| {
		if ev == Event::KeyDown
		&& app::event_key() == Key::Enter {
			let mut stream = TcpStream::connect(addr).unwrap();
			let key = [i.value(), room.clone()].join("\n");
			stream.write_all(key.as_bytes()).unwrap();
			let string = read_reply(&mut stream);
			output.buffer().unwrap().append(&string);
			output.scroll(
				output.count_lines(
					0,
					output.buffer().unwrap().length(), true
				),
				0,
			);
			i.set_value("");
			true
		} else {
			false
		}
	});
	
	
	wind.make_resizable(true);
	wind.end();
	wind.show();
	app.run().unwrap();
}
